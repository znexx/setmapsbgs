# Setmapsbgs

A quick and dirty Bash script to set the background images of your displays to random images from [Earth View](https://earthview.withgoogle.com/).

## Screenshots

Two screenshots from my dual monitor setup:

[![Screenshot 1](screenshots/screenshot_1_small.png)](screenshots/screenshot_1.png)

[![Screenshot 2, with terminal](screenshots/screenshot_2_small.png)](screenshots/screenshot_2.png)

## Features

* Simple, does what it should do. No more, no less
* Lightweight, the executable ticks in just above 2 kB
* Dependencies that are pretty normal on a regular Linux system
* Built to be compatible with Cron, for giving your desktop a fresh look every now and then

## Dependencies

* [curl](https://curl.se/) - For fetching the background images
* [feh](https://feh.finalrewind.org/) - For setting your background images. Could be replaced with whatever else you use (like `xsri`, `xsetroot`, or [`nitrogen`](https://github.com/l3ib/nitrogen))
* [the `convert` command from Imagemagick](https://imagemagick.org/script/convert.php) - For compositing the text on top of your background image
* [xrandr](https://www.x.org/wiki/Projects/XRandR/) ([on ArchWiki](https://wiki.archlinux.org/index.php/Xrandr)) - For automatically determining your number of monitors. Not used if you supply the `--screens` argument.

## Installation
1. Make sure the script is executable by running `chmod +x setmapsbgs` on it.
2. Put `setmapsbgs` in your `/bin/`, `/usr/bin/`, `~/bin/`, or any other directory in your PATH environment variable.

## Cron job

This script is made to be compatible with Cron. Here is the crontab line that I personally use, which switches my background every 5 minutes:
```
*/5 * * * * DISPLAY=:0 /home/z-nexx/bin/setmapsbgs
```

## Usage
If you have followed the installation instructions you can just run:
```
$ setmapsbgs
```

If you don't have it installed, you can of course still run it as-is with one of these commands:
```
$ ./setmapsbgs
$ bash setmapsbgs
```

## Copyright

This application is released under the `WTFPL`
```
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004

Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
 ```

 All used resources including but not limited to Earth View are copyright of their respective owners.
